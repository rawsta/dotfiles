# README #

Backup of my .files (dotfiles) 

Currently i'm [using GNU Stow](http://brandon.invergo.net/news/2012-05-26-using-gnu-stow-to-manage-your-dotfiles.html) to keep all
configurations in one place and easy rebuild my system. 

### Current Setup ###

* zsh
* vim + pathogen
* tmux
* i3wm + bumblee-status 
* Dunst 
* Ranger

### Requirements ###

* GNU Stow (for ease of use)
* ZSH (alternative Shell)
* Oh My ZSH (add-on to zsh)
* i3wm (Tiling Window Manager)
* bumblebee-status (https://github.com/tobi-wan-kenobi/bumblebee-status)
* Dunst (Notification daemon)
* w3m-img (for img/vid previews)
* Powerline
* VIM (nVIM) 
* tMux
* Terminess Powerline Font (patched Terminus Font)
* Font Awesome (icons in terminal)
* probably a lot i forgot to mention...

### Disclaimer ###

I can't guarantee that my files will work for you. I try to solve all missing dependencies as fast as possible. 
If you have problems feel free to contact me. 

### Who made these strange dotfiles? ###

* mail: rawsta@rawsta.de
* twitter: @ rawsta
* jabber: rawsta@jabber.ccc.de
